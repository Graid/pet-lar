<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 35)->unique();
            $table->text('description')->nullable();
            $table->integer('link_id', false, true)->nullable();

            $table->foreign('link_id')->references('id')->on('tags');
        });

        Schema::create('post_tags', function (Blueprint $table) {
            $table->integer('post_id', false, true);
            $table->integer('tag_id', false, true);

            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_tags');
        Schema::dropIfExists('tags');
    }
}
