<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 250)->nullable();
            $table->string('slug', 250)->nullable();
            $table->string('tags', 250)->nullable();
            $table->text('body');
            $table->integer('type_id', false, true);
            $table->integer('accepted_answer_id', false, true)->nullable();
            $table->integer('parent_id', false, true)->nullable();
            $table->integer('score')->default(0);
            $table->integer('owner_user_id', false, true);
            $table->integer('view_count', false, true)->default(0);
            $table->integer('answer_count', false, true)->default(0);
            $table->integer('comment_count', false, true)->default(0);
            $table->integer('favorite_count', false, true)->default(0);
            $table->string('owner_display_name', 40);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('owner_user_id')->references('id')->on('users');
            $table->foreign('accepted_answer_id')->references('id')->on('posts');
            $table->foreign('parent_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
