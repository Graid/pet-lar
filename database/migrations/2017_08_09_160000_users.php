<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 40);
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('reputation', false, true)->default(0);
            $table->integer('up_votes', false, true)->default(0);
            $table->integer('down_votes', false, true)->default(0);
            $table->string('avatar')->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->timestamp('last_access_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
