<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Models\Question::class, function (Faker\Generator $faker) {
    $user = User::inRandomOrder()->first();

    return [
        'title' => $faker->text(250),
        'tags' => implode(', ', $faker->words(random_int(1, 7))),
        'body' => $faker->paragraphs(random_int(1, 5), true),
        'owner_user_id' => $user->id,
        'owner_display_name' => $user->name,
    ];
});
$factory->define(App\Models\Answer::class, function (Faker\Generator $faker) {
    $user = User::inRandomOrder()->first();

    return [
        'body' => $faker->paragraphs(random_int(1, 5), true),
        'owner_user_id' => $user->id,
        'owner_display_name' => $user->name,
        'parent_id' => \App\Models\Question::inRandomOrder()->first()->id,
    ];
});