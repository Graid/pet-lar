<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Vote
 */
class Vote extends Model
{
    protected $table = 'votes';

    public $timestamps = true;

    protected $fillable = [
        'post_id',
        'type_id',
        'user_id'
    ];

    protected $guarded = [];


}