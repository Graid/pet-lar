<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PostTag
 */
class PostTag extends Model
{
    protected $table = 'post_tags';

    public $timestamps = false;

    protected $fillable = [
        'post_id',
        'tag_id'
    ];

    protected $guarded = [];


}