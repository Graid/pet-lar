<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 */
abstract class Post extends Model
{
    use Sluggable;

    const TYPE_QUESTION = 1;
    const TYPE_ANSWER = 2;

    protected $table = 'posts';

    public function __construct(array $attributes = [])
    {
        $this->setRawAttributes(['type_id' => static::getPostType()], true);
        parent::__construct($attributes);
    }

    public $timestamps = true;

    protected $fillable = [
        'title',
        'slug',
        'tags',
        'body',
        'type_id',
        'accepted_answer_id',
        'parent_id',
        'score',
        'owner_user_id',
        'view_count',
        'answer_count',
        'comment_count',
        'favorite_count',
        'owner_display_name'
    ];

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('typeId', function (Builder $builder) {
            $builder->where('type_id', '=', static::getPostType());
        });
    }

    abstract static protected function getPostType(): int;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function tags()
    {
        return $this->hasManyThrough(Tag::class, PostTag::class);
    }

    public function save(array $options = [])
    {
        $this->tags()->create(array_combine(',',explode(',', $this->tags)));
        parent::save();
    }
}