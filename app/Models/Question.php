<?php

namespace App\Models;

/**
 * Class Question
 */
class Question extends Post
{
    static protected function getPostType(): int
    {
        return Post::TYPE_QUESTION;
    }
}