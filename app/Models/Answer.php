<?php

namespace App\Models;

/**
 * Class Answer
 */
class Answer extends Post
{
    static protected function getPostType(): int
    {
        return Post::TYPE_ANSWER;
    }
}