<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 */
class Comment extends Model
{
    protected $table = 'comments';

    public $timestamps = true;

    protected $fillable = [
        'text',
        'post_id',
        'score',
        'user_id',
        'user_display_name'
    ];

    protected $guarded = [];


}