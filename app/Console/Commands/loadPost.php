<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class loadPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'StackOverflow:LoadPost';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stack Overflow';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        $parameters = [
            'pagesize' => '2',
            'order' => 'desc',
            'sort' => 'activity',
            'q' => ['russian'],
            'site' => 'travel'
        ];
        $response = $client->get('api.stackexchange.com/2.2/search/advanced', ['query' => $parameters]);
        var_dump($response);
    }
}
