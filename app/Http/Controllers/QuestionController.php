<?php

namespace App\Http\Controllers;

use App\Models\Question;

class QuestionController extends Controller
{
    public function list()
    {
        $questions = Question::orderBy('id','DESC')->paginate(10);
        return view('question.list', compact('questions'));
    }

    public function create()
    {

    }

    public function show(int $id, string $slug)
    {
        $question = Question::find($id);
        if ($slug !== $question->slug) {
            return redirect(route('show', ['id' => $question->id, 'slug'=> $question->slug]));
        }
        var_dump($question);
    }
}
