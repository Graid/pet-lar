@extends('layouts.default')

@section('content')
    <?php /** @var App\Models\Question $question */ ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Items CRUD</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('create') }}"> Create New Item</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        @foreach ($questions as $key => $question)
            <tr>
                <td>{{ $question->score }} голосов</td>
                <td>{{ $question->answer_count }} ответов</td>
                <td>{{ $question->view_count }} показов</td>
                <td>
                    <a href="{{ route('show', ['id' => $question->id, 'slug'=> $question->slug]) }}">{{ $question->title }}</a>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $questions->render() !!}

@endsection